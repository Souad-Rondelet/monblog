<?php

namespace App\Controller;

use App\Entity\Ad;
use App\Repository\AdRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class AdController extends AbstractController
{
    /**
     * @Route("/ads", name="ads_index")
     */
    public function index(AdRepository $repo)
    {
        //recuperer mon repository 
        //au lieux d'écrire cette ling on utilise les injections de dipondnace 
      //  $repo = $this->getDoctrine()->getRepository(Ad::class);
        //crée une variable add en plurieuls pour recupérer tous les add et demander au répo d'aller récuperer 
        //tous les enregistrements de la table visé
        $ads = $repo->findAll();
        return $this->render('ad/index.html.twig', [
            'ads' => $ads //on luis passe un variable qui s'appel adds
        ]);
    }
    /**
     * Undocumented function
     * 
     * @Route("/ads/{slug}", name="ads_show")
     *
     * @return Response 
     */

    public function show(Ad $ad){
        //je récupère l'annonce qui correspond au slug !
      //  $ad = $repo->findOneBySlug($slug);
      //grace au parame converter 

        return $this->render('ad/show.html.twig', [
            'ad' => $ad
        ]);
    }

}
