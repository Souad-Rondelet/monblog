<?php

namespace App\Controller;

use App\Entity\Utilisateur;
use App\Form\RegistrationType;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AccountController extends AbstractController
{
    /**
     * Permet d'afficher et de gérer le formulaire de connexion 
     * @Route("/login", name="account_login")
     * @return  Response
     */
    public function login(AuthenticationUtils $utils)
    //cela permet d'authetifier l'utilisateur
    {
        $error = $utils->getLastAuthenticationError();
        //permet de récupere ou de garder l'adress mail de l'utilisateur qui essaye de se connecter 
        $username = $utils->getLastUsername();

        return $this->render('account/login.html.twig', [
            'hasError' => $error !== null, //si different du null sino il ya pas d'error
            'username' =>$username
            
        ]);
    }
    /**
     * Permet de se déconnecter
     * 
     * @Route("/logout", name="account_logout") 
     * @return void
     */
    public function logout(){

    }

    /**
     * Permet d'afficher le formulaire d'inscription
     * 
     *@Route("/register", name="account_register")

     * @return Response
     */
    //ensuite creer le formulaire d'enregistrement
    public  function register(Request $request, ObjectManager $manager, UserPasswordEncoderInterface $encoder)
    {

       $utilisateur = new Utilisateur();
       $form = $this->createForm(RegistrationType::class, $utilisateur);

       $form->handleRequest($request);

       if($form->isSubmitted() && $form->isValid()){
           //demander un encoder pour le mot de passe 
           //dans les paramètres o lui donne le mot de pass quand a données hashé au utilisateur
           $password = $encoder->encodePassword($utilisateur, $utilisateur->getPassword());
           $utilisateur->setPassword($password);

           $manager->persist($utilisateur);
           $manager->flush();

           $this->addFlash(
               'success', 
               "Votre compte à bien été crée! Vous pouvez vous connectez"
           );
           return $this->redirectToRoute('account_login');

       }
       return $this->render('account/registration.html.twig', [
           'form' => $form->createView()
       ]);

       
    }
}
