<?php

namespace App\DataFixtures;

use App\Entity\Ad;
use Faker\Factory;
use App\Entity\Image;
use App\Entity\Utilisateur;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
  private $encoder;
  public function __construct(UserPasswordEncoderInterface $encoder)
  {
    $this->encoder = $encoder;
  }



  public function load(ObjectManager $manager)
  {
    //pour utiliser faker 
    //factory c'est une classe
    $faker = Factory::create('fr-FR');
    //nous gerons les utilisateurs
    $utilisateurs = [];

    $genres = ['male', 'female'];

    for ($i = 1; $i <= 10; $i++) {
      $utilisateur = new Utilisateur();

      $genre = $faker->randomElement($genres);

      
      $picture = 'https://randomuser.me/api/portraits/';
      $pictureId = $faker->numberBetween(1, 99) . '.jpg';
      $picture .= ($genre == 'male' ? 'men/' : 'women/') . $pictureId;

      $utilisateur->setFirstName($faker->firstName($genres))
                  ->setLastName($faker->lastName)
                  ->setEmail($faker->email)
                  ->setPassword('password')
                  ->setPicture($picture);


      $manager->persist($utilisateur);
      $utilisateurs[] = $utilisateur;
    }


    //nous gerons les AD
    //on fait la relation avec les annonces


    for ($i = 1; $i <= 14; $i++) {

      // $password $this->encoder->encodePassword($utilisateur, 'password');
      //modifier l'entité user ajouter 
      // impliment user interface
      //crrer une anonnce 
      $ad = new Ad();

      //va metter de lorem on aura jamais le meme titre 
      $title = $faker->sentence();

      $CoverImage = $faker->imageUrl(1000, 350);
      $introduction = $faker->paragraph(2);
      //demander de faire plusieurs paragraph 
      $content = '<p>' . join('</p><p>', $faker->paragraphs(5)) . '</p>';


      //permet d'ajouter et compter les utilisateur ajouter dans le tableau des utilisateurs
      $utilisateur = $utilisateurs[mt_rand(0, count($utilisateurs) - 1)];

      //un annoce a un title setter
      $ad->setTitle($title)

        ->setCoverImage($CoverImage)
        ->setIntroduction($introduction)
        ->setContent($content)
        //mt_rand() permet d'avoir deux numero aleatoir 
        ->setPrice(mt_rand(40, 200))
        ->setRooms(mt_rand(1, 5))
        ->setAuthor($utilisateur);


      for ($j = 1; $j <= mt_rand(2, 5); $j++) {
        $image = new Image();

        $image->setUrl($faker->imageUrl())
          ->setCaption($faker->sentence())
          ->setAd($ad);
          
        $manager->persist($image);
        // achque fois je persiste l'image je persist le ad

      }
      $manager->persist($ad);
    }
    //balance la requete
    $manager->flush();
    //cette étape permet d'ajouter les données dans la base de données
    //Le Repository permet de faire les SELECTION 
    //Le manager permet de faire des manipulation
  }
}
